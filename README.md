
# Angular Electron App

This project combines Angular and Electron to create a desktop application.

## Prerequisites

Make sure you have the following installed:

- [Node.js](https://nodejs.org) (version v18.16.1 or above)
- [Angular CLI](https://angular.io/cli) (version 14.X.X or above)

## Getting Started

1. Clone this repository:

   ```shell
   git clone https://gitlab.com/syafiqshamsuddin96/angular-electron-interview-test.git
   ```

2. Navigate into the project directory:

   ```shell
   cd angular-electron-interview-test
   ```

3. Install the dependencies:

   ```shell
   npm install
   ```

4. Start the Electron app in development mode:

   ```shell
   npm start
   ```

## Project Structure

- `app`: Contains the Electron main process folder (Node.js).
- `src`: Contains the Electron renderer process folder (Web / Angular).

## Learn More

To learn more about Angular and Electron, visit the following official documentation:

- [Angular Documentation](https://angular.io/docs)
- [Electron Documentation](https://www.electronjs.org/docs)
