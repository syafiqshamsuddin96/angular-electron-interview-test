import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private apiUrl = 'http://test-demo.aemenersol.com/api/account/login';
  private tokenKey = 'auth_token';

  private authState = new BehaviorSubject<boolean>(this.isLoggedIn());


  constructor(private http: HttpClient) {}

  login(username: string, password: string) {
    const loginData = {
      username,
      password,
    };

    return this.http.post<string>(this.apiUrl, loginData);
  }

  setAuthToken(token: string): void {
    localStorage.setItem(this.tokenKey, token);
    this.authState.next(true);
  }

  getAuthToken(): string | null {
    return localStorage.getItem(this.tokenKey);
  }

  clearAuthToken(): void {
    localStorage.removeItem(this.tokenKey);
    this.authState.next(false);
  }

  isLoggedIn(): boolean {
    return !!this.getAuthToken();
  }

  getAuthState(): Observable<boolean> {
    return this.authState.asObservable();
  }
}
