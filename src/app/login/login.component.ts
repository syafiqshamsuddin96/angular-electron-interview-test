import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loading = false;
  formError: string | null = null;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  get formControls() {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
    this.authService.getAuthState().subscribe(isLoggedIn => {
      if (isLoggedIn) {
        this.router.navigate(['/home']);
      }
    });
  }

  login() {
    this.submitted = true;

    if (!this.loginForm.valid) {
      return;
    }
    this.loading = true;

    // Perform authentication logic here
    const { username, password } = this.loginForm.value;

    this.authService.login(username, password).subscribe(
      (token) => {
        this.authService.setAuthToken(token);
      },
      (error) => {
        // Handle the error response
        console.error('Login error:', error);
        this.formError = 'Failed to login';
        this.loading = false;
      }
    );
  }
}
